var express = require('express');
var app = express();

var data = [];

app.use(express.json());
app.use((req, res, next) => {
    console.log('LOG: ', req.url, req.method);
    next();
});

app.get('/item/all', (req, res) => {
    res.send(data);
});

app.get('/item/:index', (req, res) => {
    res.send(data[+req.params.index]);
});

app.post('/item', (req, res) => {
    data.push(req.body);
    res.status(201).send(data);
});

app.put('/item/:index', (req, res) => {
    data[+req.params.index] = req.body;
    res.send(data);
});

app.delete('/item/:index', (req, res) => {
    data.splice(+req.params.index, 1);
    res.send(data);
});

app.listen(8080);